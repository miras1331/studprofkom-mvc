var gulp = require('gulp')
var sass = require('gulp-sass')
var autoprefixer = require('gulp-autoprefixer')

gulp.task('sass', function () {
  gulp.src('./studProfkom/Content/Styles/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./studProfkom/Content/Styles'))
})

gulp.task('sass:watch', function () {
  gulp.watch('./studProfkom/Content/Styles/*.scss', ['sass']);
})
